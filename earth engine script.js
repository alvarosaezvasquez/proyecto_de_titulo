//Script para descargar mapa desde el editor de código de Earth Engine

var region_list = ee.List(["","Cauquenes"]);
var region_filter = ee.Filter.inList("ADM2_NAME", region_list);
var region_geometry = ee.FeatureCollection("FAO/GAUL/2015/level2").filter(region_filter).geometry();

print(region_geometry);
Map.addLayer(region_geometry, {}, 'Second Level Administrative Units');

var startDate = '2023-01-01';
var endDate = '2023-02-01';
var dw = ee.ImageCollection('GOOGLE/DYNAMICWORLD/V1')
    .filterDate(startDate, endDate).filterBounds(region_geometry);
var dwclipped = dw.map(function(image) {
  return image.clip(region_geometry);
});

var dwImage = ee.Image(dwclipped.mosaic());
print('DW ee.Image', dwImage);

var classification = dwImage.select('label');
var dwVisParams = {
  min: 0,
  max: 8,
  palette: ['#419BDF', '#397D49', '#88B053', '#7A87C6',
  '#E49635', '#DFC35A', '#C4281B', '#A59B8F',
  '#B39FE1']
};
Map.addLayer(classification, dwVisParams, 'Classified Image');

Export.image.toDrive({
  image: classification,
  description: "cauquenes_2023",
  scale: 30,
  region: region_geometry,
  maxPixels: 1e13,
  skipEmptyTiles: true
});